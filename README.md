# Graphs Webapp (Micro Frontend Architecture)

1. Install Docker Compose.
2. Run the following command at root of project.
    ```
    docker-compose up
    ```
3. View app (http://localhost:3000)
import { LitElement, html } from '@polymer/lit-element/lit-element.js';
import styles from './styles.css';
import '@polymer/iron-pages/iron-pages.js'

class DashboardPage extends LitElement {
  static get properties() {
    return {
      page: {
        type: String
      }
    }
  }

  constructor() {
    super()
  }

  _render() {
    return html`
      <style>
        ${styles}
      </style>
      <div class="viewport">
        <div class="menu-bar">
          <div class="menu-bar__app">
            <p class="menu-bar__app-name">Web Analytics</p>
          </div>
          <div class="menu-bar__item">
            <a href="/graph-viewer">
              <p class="menu-bar__item-text">Graph Viewer</p>
            </a>
          </div>
          <div class="menu-bar__item">
            <a href="/graph-editor">
              <p class="menu-bar__item-text">Graph Editor</p>
            </a>
          </div>
        </div>
        <iron-pages attr-for-selected="page" class="iron-pages" selected="${this.page}">
          <graph-editor page="editor"></graph-editor>
          <graph-viewer page="viewer"></graph-viewer>
        </iron-pages>
      </div>
    `
  }
}

export default DashboardPage;
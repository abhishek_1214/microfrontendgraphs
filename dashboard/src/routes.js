import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', {page: "viewer"})
})

router.get('/graph-viewer', (req, res) => {
  res.render('index', {page: "viewer"})
})

router.get('/graph-editor', (req, res) => {
  res.render('index', {page: "editor"})
})

export default router;
import express from 'express';
import morgan from 'morgan';
import path from 'path';
import routes from './routes';

const app = express();

app.use(morgan('dev'));

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

app.use('/dashboard/images', express.static('./images'))
app.use('/dashboard', express.static('./build'))

app.use('/', routes)


app.listen(3001)
console.log(`🔴  dashboard running. dashboard available here:
>> http://127.0.0.1:3001/`);
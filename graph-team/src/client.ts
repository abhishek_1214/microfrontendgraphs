import 'zone.js/dist/zone';
import 'chart.js/src/chart.js';
import 'rxjs';

import '@webcomponents/custom-elements/src/native-shim';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { GraphTeamModule } from './graph-team.module';

platformBrowserDynamic().bootstrapModule(GraphTeamModule);
import { Component, OnInit, Input } from "@angular/core";
import { GraphDataItem } from "../graph-viewer/graph-viewer.models";

@Component({
  selector: 'graph-card',
  styles: [`
    .graph-wrapper {
      margin: 16px;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: calc(100% - 32px);
    },
  `],
  template: `
  <div class="graph-wrapper">
    <ngx-charts-bar-vertical
      class="graph-container"
      *ngIf="graphType === 'bar'"
      [scheme]="colorScheme"
      [results]="data"
      [gradient]="gradient"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [legend]="showLegend"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel"
      (select)="onSelect($event)">
    </ngx-charts-bar-vertical>

    <ngx-charts-pie-chart
      *ngIf="graphType === 'pie'"
      class="graph-container"
      [scheme]="colorScheme"
      [results]="data"
      [gradient]="gradient"
      [legend]="showLegend"
      (select)="onSelect($event)">
    </ngx-charts-pie-chart>

    <ngx-charts-line-chart
      *ngIf="graphType === 'line'"
      class="graph-container"
      [scheme]="colorScheme"
      [results]="data"
      [gradient]="gradient"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [legend]="showLegend"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel"
      (select)="onSelect($event)">
    </ngx-charts-line-chart>
  </div>
  `,
})
export class GraphCardComponent implements OnInit {
  @Input() graphType: string;
  @Input() data: GraphDataItem[]
  multi: any[];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  ngOnInit() {
  }

  onSelect(event) {
    console.log(event);
  }
}
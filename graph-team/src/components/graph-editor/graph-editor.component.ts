import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { GraphDataItem } from "../graph-viewer/graph-viewer.models";
import { FirebaseService } from "../../services/firebase.service";

@Component({
  selector: 'graph-editor',
  styles: [`
    :host {
      display: flex;
      flex: 1;
      overflow-y: scroll;
    }

    p {
      margin: 0;
      padding: 0;
    }

    .action-bar {
      background: #fff;
      display: flex;
      align-items: center;
      flex-direction: row;
      height: 56px;
    }

    .add-button {
      margin-top: 16px;
    }

    .page-title {
      margin: 0;
      padding: 0;
      margin-left: 16px;
      font-size: 1.4rem;
      flex: 1;
    }

    .content {
      padding: 16px 16px 0 16px;
    }

    .card {
      display: inline-block !important;
      margin-right: 16px;
      max-width: 380px;
    }

    .card-content {
      display: flex;
      flex-direction: column;
      padding: 8px 16px;
      border-bottom: 1px solid #dedede;
    }

    .card-heading {
      margin: 8px 0;
      padding: 0;
      font-size: 12px;
      font-weight: 500;
      color: #777;
    }

    .card-row {
      display: flex;
      flex-direction: row;
      align-items: center;
    }

    .viewport {
      display: block;
      height: 100vh;
      width: 100%;
    }

    .card-row > * {
      margin-right: 8px;
    }

  `],
  styleUrls: [ '../../../node_modules/@angular-mdc/theme/material.scss'],
  template: `
    <div class="viewport">
      <div class="action-bar">
        <p class="page-title">Graph Editor</p>
        <button (click)="saveData()" mdc-button>Save Changes</button>
      </div>
      <div class="content">
        <mdc-card class="card" *ngFor="let item of items; let count = index">
          <div class="card-content">
            <p class="card-heading">Graph Id: {{item.key}}</p>
            <mdc-select dense [(ngModel)]="item.graphType" placeholder="Select type of Graph">
              <option value="line">Line</option>
              <option value="bar">Bar</option>
              <option value="pie">Pie</option>
            </mdc-select>
          </div>
          <div class="card-content">
            <p class="card-heading">Graph Position</p>
            <div class="card-row">
              <mdc-text-field [(ngModel)]="item.x" type="number" class="row-input" label="X"></mdc-text-field>
              <mdc-text-field [(ngModel)]="item.y" type="number" class="row-input" label="Y"></mdc-text-field>
            </div>
            <div class="card-row">
              <mdc-text-field [(ngModel)]="item.cols" type="number" class="row-input" label="width"></mdc-text-field>
              <mdc-text-field [(ngModel)]="item.rows" type="number" class="row-input" label="height"></mdc-text-field>
            </div>
          </div>
          <div class="card-content" *ngIf="item.graphType === 'bar' || item.graphType === 'pie'">
            <p class="card-heading">Graph Data</p>
            <div class="card-row" *ngFor="let data of item.graphData">
              <mdc-text-field label="name" dense [(ngModel)]="data.name"></mdc-text-field>
              <p>:</p>
              <mdc-text-field type="number" label="value" dense [(ngModel)]="data.value"></mdc-text-field>
            </div>
            <button (click)="addData(count)" class="add-button" mdc-button>Add Data</button>
          </div>
          <div class="card-content" *ngIf="item.graphType === 'line'">
            <p class="card-heading">Graph Data</p>
            <div class="card-series" *ngFor="let data of item.graphData; let seriesIndex = index">
              <mdc-text-field label="Series Name" dense [(ngModel)]="data.name"></mdc-text-field>
              <div class="card-row" *ngFor="let series of data.series">
                <mdc-text-field label="name" dense [(ngModel)]="series.name"></mdc-text-field>
                <p>:</p>
                <mdc-text-field type="number" label="value" dense [(ngModel)]="series.value"></mdc-text-field>
              </div>
              <button (click)="addDataInSeries(count, seriesIndex)" mdc-button>Add</button>
            </div>
            <button (click)="addData(count)" class="add-button" mdc-button>Add Data</button>
          </div>
        </mdc-card>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.Native
})
export class GraphEditorComponent implements OnInit {
  items: GraphDataItem[] = []

  constructor(private firebase: FirebaseService) {}

  addData(index: number) {
    if (this.items[index].graphType === 'line') {
      this.items[index].graphData.push({
        name: 'New Series',
        series: [
          { name: 'Fiest Val', value: 0 }
        ]
      })
    } else {
      this.items[index].graphData.push({
        name: '',
        value: 0
      })
    }
  }

  addDataInSeries(graphIndex: number, seriesIndex: number) {
    this.items[graphIndex].graphData[seriesIndex].series.push({
      name: '',
      value: 0
    })
  }

  saveData() {
    this.firebase.updateLayout(this.items);
  }

  ngOnInit() {
    this.firebase.getLayout()
      .subscribe(
        (data) => this.items = data
      )
  }
}
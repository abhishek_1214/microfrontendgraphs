import { NgModule, Injector } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { firebaseConfig } from '../../config/firebase';
import { FirebaseService } from "../../services/firebase.service";
import { GraphEditorComponent } from "./graph-editor.component";
import { createCustomElement } from "@angular/elements";
import { MdcAppBarModule, MdcIconModule, MdcCardModule, MdcSelectModule, MdcTextFieldModule, MdcButtonModule } from '@angular-mdc/web';
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    MdcAppBarModule,
    MdcIconModule,
    MdcCardModule,
    MdcSelectModule,
    MdcTextFieldModule,
    MdcButtonModule
  ],
  providers: [FirebaseService],
  declarations: [GraphEditorComponent],
  entryComponents: [GraphEditorComponent],
  bootstrap: [GraphEditorComponent]
})
export class GraphEditorModule {
  constructor(private injector: Injector) {
    const graphEditorComponent = createCustomElement(GraphEditorComponent, { injector });
    customElements.define('graph-editor', graphEditorComponent);
  }
}
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { GridsterConfig } from 'angular-gridster2';
import { FirebaseService } from '../../services/firebase.service';
import { GraphDataItem } from './graph-viewer.models';

@Component({
  selector: 'custom-button',
  template: `
    <div class="viewport">
      <div class="action-bar">
        <p class="page-title">Graph Viewer</p>
        <button (click)="saveLayout()">Save</button>
      </div>
      <gridster [options]="options">
        <gridster-item [item]="item" *ngFor="let item of items">
          <graph-card [data]="item.graphData" [graphType]="item.graphType"></graph-card>
        </gridster-item>
      </gridster>
    </div>
  `,
  styles: [`
    :host {
      display: flex;
      flex: 1;
      overflow-y: scroll;
    }

    .action-bar {
      background: #fff;
      display: flex;
      align-items: center;
      flex-direction: row;
      height: 56px;
    }

    .page-title {
      margin: 0;
      padding: 0;
      margin-left: 16px;
      font-size: 1.4rem;
      flex: 1;
    }

    .viewport {
      display: flex;
      flex: 1;
      height: 100vh;
      flex-direction: column;
    }
  `],
  encapsulation: ViewEncapsulation.Native
})
export class GraphViewerComponent implements OnInit {
  options: GridsterConfig;
  items: GraphDataItem[] = [];
  
  static onItemResize() {
    window.dispatchEvent(new CustomEvent('resize'))
  }

  constructor(private firebaseService: FirebaseService) {
    this.options = {
      draggable: {},
      gridType: 'verticalFixed',
      minCols: 6,
      maxCols: 6,
      itemResizeCallback: GraphViewerComponent.onItemResize,
      compactType: 'none',
      resizable: {
        enabled: true,
        handles: {
          n: false,
          s: false,
          w: false,
          e: false,
          nw: true,
          sw: true,
          se: true,
          ne: true
        }
      }
    }
  }

  ngOnInit() {
    this.firebaseService.getLayout()
      .subscribe(
        (val) => { this.items = val }
      )
  }

  saveLayout() {
    if (this.items) {
      this.firebaseService.updateLayout(this.items);
    }
  }
}
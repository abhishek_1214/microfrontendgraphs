import { GridsterItem } from "angular-gridster2";

interface SeriesData {
  name: string;
  value: number;
  min?: number;
  max?: number;
}

interface GraphData {
  name: string;
  value?: number;
  series?: SeriesData[]
}

export interface GraphDataItem extends GridsterItem {
  graphType: 'pie' | 'bar' | 'line';
  key?: string;
  graphData: GraphData[];
}
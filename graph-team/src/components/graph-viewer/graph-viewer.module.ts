import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridsterModule } from 'angular-gridster2';
import { AngularFireModule } from 'angularfire2';
import { GraphViewerComponent } from './graph-viewer.component';
import { BrowserModule } from '@angular/platform-browser';
import { firebaseConfig } from '../../config/firebase';
import { FirebaseService } from '../../services/firebase.service';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GraphCardComponent } from '../graph-card/graph-card.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    GridsterModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    NgxChartsModule
  ],
  providers: [FirebaseService],
  declarations: [
    GraphViewerComponent,
    GraphCardComponent
  ],
  entryComponents: [GraphViewerComponent],
  bootstrap: [GraphViewerComponent]
})
export class GraphViewerModule {
  constructor(private injector: Injector) {
    const graphViewer = createCustomElement(GraphViewerComponent, { injector });
    customElements.define('graph-viewer', graphViewer);
  }

  ngDoBootstrap() {}
}
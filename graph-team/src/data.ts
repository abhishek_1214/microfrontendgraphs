import { GraphDataItem } from "./components/graph-viewer/graph-viewer.models";

export const data: GraphDataItem[] = [
  { 
    x: 0, 
    y: 0, 
    cols: 2,
    rows: 2, 
    graphType: 'bar', 
    graphData: [ 
      { name: 'Germany', value: 4000 },
      { name: 'USA', value: 6000 }
    ]
  },
  { 
    x: 2, 
    y: 0, 
    cols: 2,
    rows: 2, 
    graphType: 'pie', 
    graphData: [ 
      { name: 'Germany', value: 4000 },
      { name: 'USA', value: 6000 }
    ]
  },
  { 
    x: 4, 
    y: 0, 
    cols: 2,
    rows: 2, 
    graphType: 'line', 
    graphData: [ 
      { 
        name: 'Germany', 
        series: [
          {
            "name": "2010",
            "value": 7300000,
          },
          {
            "name": "2011",
            "value": 1568460,
          }
        ]
      },
      { 
        name: 'USA', 
        series: [
          {
            "name": "2010",
            "value": 7304561,
          },
          {
            "name": "2011",
            "value": 8940000,
          }
        ]
      },
    ]
  }
];
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { GraphViewerModule } from './components/graph-viewer/graph-viewer.module';
import { GraphEditorModule } from './components/graph-editor/graph-editor.module';

@NgModule({
  imports: [BrowserModule, GraphViewerModule, GraphEditorModule],
})
export class GraphTeamModule {
  ngDoBootstrap() {}
}
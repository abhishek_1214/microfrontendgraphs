import express from 'express';
import morgan from 'morgan';

const app = express();

app.use(morgan('dev'));

app.use('/graph-team/images', express.static('./images'))
app.use('/graph-team', express.static('./dist'))

app.listen(3002);
console.log(`💚  graph-viewer running. fragments are available here:
>> http://127.0.0.1:3002/graph-viewer`);
import { Injectable } from "@angular/core";
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { Observable } from "rxjs";
import { GraphDataItem } from "../components/graph-viewer/graph-viewer.models";
import { map } from 'rxjs/operators'

@Injectable()
export class FirebaseService {
  layoutRef: AngularFireList<any>;
  constructor(private db: AngularFireDatabase) {
    this.layoutRef = db.list('/layout');
  }

  getLayout(): Observable<GraphDataItem[]> {
    return this.layoutRef.snapshotChanges()
      .pipe(
        map(changes => 
            changes.map(item => ({
              ...item.payload.val(),
              key: item.payload.key
            }))
        )
      )
  }

  updateLayout(data: GraphDataItem[]) {
    let formated = data.map(item => {
      this.layoutRef.update(item.key, {
        ...item,
        key: null
      })
    })
  }

  addLayout(data: GraphDataItem[]) {
    let formated = data.map(item => {
      this.layoutRef.push(item)
    })
  }
}
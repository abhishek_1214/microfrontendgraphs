const NgCompilerPlugin = require('@ngtools/webpack').AngularCompilerPlugin
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  entry: './src/client.ts',
  output: {
    filename: 'fragments.js',
  },
  module: {
    rules: [
      {test: /\.ts$/, exclude: /node_modules/, loader: '@ngtools/webpack'},
      {test: /\.html$/, loader: 'raw-loader'},
      {test: /\.css$/, loader: 'raw-loader'},
      {test: /\.scss$/, loader: ['raw-loader', 'sass-loader']},
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  plugins: [
    new NgCompilerPlugin({
      tsConfigPath: './tsconfig.json',
      mainPath: './src/client.ts',
    }),
    new UglifyJSPlugin(),
  ],
  mode: 'production',
  stats: 'errors-only'
};
